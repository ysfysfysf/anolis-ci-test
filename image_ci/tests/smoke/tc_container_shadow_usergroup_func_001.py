#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_shadow_usergroup_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class UsergroupTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_usergroup_operation(self):
        self.cmd('groupdel test_group',ignore_status=True)
        self.cmd('groupadd test_group')
        output0 = self.cmd('tail -n1 /etc/group | grep test_group')
        self.assertTrue('test_group' in output0,"create test_group group failed")
        self.cmd('groupmod -n test_groups test_group')
        output = self.cmd('cat /etc/group | grep test_groups')
        self.assertTrue('test_groups' in output,"Fail to modify the name of usergroup")
        self.cmd('groupdel test_groups',ignore_status=True)
        output1 = self.cmd('tail -n1 /etc/group | grep test_groups',ignore_status=True)
        self.assertEqual(output1,'',"delete test_groups failed")