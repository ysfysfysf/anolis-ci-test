#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_yum_service_base.py
@Time:      2022/8/31 10:06:45
@Author:    liuyaqing
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class YumBaseTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_yum(self):
        output1 = self.cmd('yum clean all')
        output2 = self.cmd('yum makecache')
        output3 = self.cmd('yum update -y')