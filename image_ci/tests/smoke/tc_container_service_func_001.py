#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_service_func_001.py
@Time:      2022/9/24 17:06:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ContainServTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,app_container
    """

    def test_container_service(self):
        self.skip('Service related cases are temporarily unavailable')
        self.container_flag = 1
        container_service = self.image_name.split('/')[-1]
        output = int(self.cmd('rpm -qa | grep '+container_service+' | wc -l'))
        self.assertTrue(output > 0,"Available packages are not installed")
        output1 = self.cmd('systemctl list-unit-files | grep '+container_service)
        self.assertTrue('active' not in output1,"The service status in the application container is abnormal")