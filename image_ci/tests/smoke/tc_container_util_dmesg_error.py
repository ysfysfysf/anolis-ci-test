#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_util_dmesg_error.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

error_list=['integrity: Unable to open file: /etc/keys/x509_evm.der',
            'integrity: Unable to open file: /etc/keys/x509_ima.der',
            'SDEI NMI watchdog: Disable SDEI NMI Watchdog in VM',
            'SELinux:  Runtime disable is deprecated, use selinux=0 on the kernel cmdline.',
            'kvm-guest: host does not support poll control',
            'kvm-guest: host upgrade recommended',
            'Failed to check link status','IRQ index',
            'sp5100-tco sp5100-tco: Watchdog hardware is disabled','Could not read FW version','FW version command failed',
            'Spectre v2 mitigation leaves CPU vulnerable to RETBleed attacks','no codecs found!',
            'too much work for irq4','Spectre v2 : WARNIG: Unprivileged eBPF is enabled with eIBRS on, data leaks possible via Spectre v2 BHB attacks!',
            'Spectre v2 mitigation leaves CPU vulnerable to RETBleed attacks, data leaks possible!','microcode: patch size mismatch']

class DmesgErrorTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """

    def test_dmesg_err(self):
        output_error_list=[]
        output1 = self.cmd('dmesg -l err -T | grep -Ev "^$|#"',ignore_status=True)
        self.log.debug(output1)
        if output1 is '':
            self.log.debug('There is no error information')
        else:
            for line in output1.split('\n'):
                for i in range(len(error_list)):
                    if error_list[i] not in line and i == len(error_list) - 1:
                        output_error_list.append(line)
                    elif error_list[i] in line:
                        break
            self.assertEqual(len(output_error_list),0,'The error dmesg: {}'.format(output_error_list))
            self.log.debug('There is no other error information')