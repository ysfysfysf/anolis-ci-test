#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_process_func_001.py
@Time:      2022/9/27 19:06:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ContainProcTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,app_container
    """

    def test_container_process(self):
        self.container_flag = 0
        output = self.cmd(self.container_engine+' inspect '+self.container_id+' | grep \"Pid\" | awk -F ":" \'{print$2}\'')
        container_pid = output.split(',')[0]
        self.assertTrue(container_pid is not None,"Application image has no corresponding process")
        container_proc = self.cmd(self.container_engine+' top '+self.container_id+' | grep '+container_pid)
        self.assertTrue(container_proc is not None,"No valid process started in container")