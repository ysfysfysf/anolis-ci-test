#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_gcc_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class GccTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_gcc_compile(self):
        self.cmd('rm -rf anolis-sys-tests-res hello1')
        self.cmd('yum install git gcc -y')
        self.cmd('git clone http://gitee.com/yunmeng365524/anolis-sys-tests-res.git')
        self.cmd('gcc anolis-sys-tests-res/res/hello.c -o hello1')
        output1 = self.cmd('ls | grep hello1')
        self.assertEqual(output1,"hello1","gcc compile failed!")
        self.cmd('chmod +x hello1')
        output2 = self.cmd('./hello1')
        self.assertEqual(output2, "Hello World!","gcc output is not true!")
    def tearDown(self):
        super().tearDown()
        self.cmd('yum remove git gcc -y')
        self.cmd('rm -rf anolis-sys-tests-res hello1')