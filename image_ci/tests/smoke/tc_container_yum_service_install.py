#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_yum_service_install.py
@Time:      2022/8/31 10:06:45
@Author:    liuyaqing
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
"""

from common.basetest import BaseTest
from common.containertest import ContainerTest
class YumGroupTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_yum_devel(self):
        self.cmd('yum install java nodejs golang python3 python2 maven -y')
        output = self.cmd('yum group list | grep  "Available Groups:" -A 5')
        group_name = output.split('\n')[-2]
        self.log.debug(group_name.strip())
        group_inst_cmd = 'yum group install "'+group_name.strip()+'" -y'
        group_rmv_cmd = 'yum group remove "'+group_name.strip()+'" -y'
        self.cmd(group_inst_cmd)
        self.cmd(group_rmv_cmd)
    def tearDown(self):
        super().tearDown()
        self.cmd('yum remove java nodejs golang python3 python2 maven -y')