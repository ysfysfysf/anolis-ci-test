#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_util_dmesg_warn.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

warn_list=["cacheinfo: Unable to detect cache hierarchy for CPU",
            "output lines suppressed due to ratelimiting",
            "fail to add MMCONFIG information, can't access extended PCI configuration space under this bridge.",
            "journal corrupted or uncleanly shut down, renaming and replacing",
            "GPT: Use GNU Parted to correct GPT errors",
            "process 'local/aegis/aegis_update/AliYunDunUpdate' started with executable stack",
            "NFSD: Using nfsdcld client tracking operations.",
            "MDS CPU bug present and SMT on, data leak possible.",
            "TAA CPU bug present and SMT on, data leak possible.",
            "Decoding supported only on Scalable MCA processors",
            "TSC doesn't count with P0 frequency",
            "Unknown NUMA node; performance will be reduced",
            "KASLR disabled due to lack of seed",
            "Ignoring unsafe software power cap!",
            "hwmon_device_register() is deprecated. Please convert the driver to use hwmon_device_register_with_info()",
            "You have booted with nomodeset. This means your GPU drivers are DISABLED",
            "Any video related functionality will be severely degraded, and you may not even be able to suspend the system properly",
            "Unless you actually understand what nomodeset does, you should reboot without enabling it",
            "ENERGY_PERF_BIAS: Set to 'normal', was 'performance'",
            "ENERGY_PERF_BIAS: View and update with x86_energy_perf_policy",
            "MMIO Stale Data CPU bug present and SMT on, data leak possible.",
            "usb: port power management may be unreliable",
            "ACPI PPTT: No PPTT table found, CPU and cache topology may be inaccurate",
            "Configuration file /etc/systemd/system/cloudmonitor.service is marked executable.",
            "Volume was not properly unmounted. Some data may be corrupt","No controller found","SELinux: unrecognized netlink message",
            "with a huge-page mapping due to MTRR override.","Unknown lvalue 'ConditionCPUs' in section 'Unit'",
            "GPT:Primary header thinks Alt. header is not at the end of the disk.",
            "lacks a native systemd unit file. Automatically generating a unit file for compatibility.",
            "No MFD cells added","failed stop FIS RX","ib_ucm: disagrees about version of symbol","can't derive routing for PCI INT",
            "The BMC does not support clearing the recv irq bit, compensating, but the BMC needs to be fixed."]

class DmesgWarnTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_dmesg_warn(self):
        output_warn_list=[]

        output2 = self.cmd('dmesg -l warn -T | grep -Ev "^$|#"',ignore_status=True)
        self.log.debug(output2)
        if output2 is '':
            self.log.debug('There is no warning information')
        else:
            for line in output2.split('\n'):
                for i in range(len(warn_list)):
                    if warn_list[i] not in line and i == len(warn_list) - 1:
                        output_warn_list.append(line)
                    elif warn_list[i] in line:
                        break
            self.assertEqual(len(output_warn_list),0,'The warn dmesg: {}'.format(output_warn_list))
            self.log.debug('There is no other warning information')