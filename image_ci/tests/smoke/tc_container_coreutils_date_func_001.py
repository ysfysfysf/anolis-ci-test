#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_coreutils_date_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng
@Version:   1.0   
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest
import datetime
import ntplib

class DateTest(ContainerTest):
    def test_systime(self):
        """
        :avocado: tags=P1,noarch,local,baseos_container
        """
        timegap=60
        client = ntplib.NTPClient()
        response = client.request('ntp.aliyun.com')
        t1 = datetime.datetime.fromtimestamp(response.tx_time)
        localtime = int(self.cmd("date +'%s'"))
        t2 = datetime.datetime.fromtimestamp(localtime)
        self.log.debug("t1 represents the precise time obtained from the ntp server: %s", t1)
        self.log.debug("t2 indicates the current time of the test machine: %s", t2)
        td1 = t2 - t1
        self.assertTrue(abs(td1.total_seconds()) < timegap,"timegap between t1 and t2 over 60s")