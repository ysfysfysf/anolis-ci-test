#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_startup_func_001.py
@Time:      2022/9/20 18:06:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class StartupTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,container,app_container
    """

    def test_container_startup(self):
        lang = self.cmd('echo $LANG')
        self.assertTrue(lang is 'zh_CN.UTF-8' or 'C.utf8',"The initial encoding language of the container is not as expected!")
        self.container_flag = 0
        host_kversion = self.cmd('uname -r')
        self.container_flag = 1
        container_kversion = self.cmd('uname -r')
        self.assertEqual(host_kversion,container_kversion,"The container kernel version is not as expected!")
        