#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_rust_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""

from common.basetest import BaseTest
from common.containertest import ContainerTest

class RustTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_rust_compile(self):
        self.cmd('yum install git rust -y')
        self.cmd('git clone http://gitee.com/yunmeng365524/anolis-sys-tests-res.git')
        self.cmd('rustc anolis-sys-tests-res/res/helloworld.rs')
        output1 = self.cmd('ls | grep helloworld')
        self.assertEqual(output1.split('\n')[0],"helloworld","rust compile failed!")
        self.cmd('chmod +x helloworld')
        output2 = self.cmd('./helloworld')
        self.assertEqual(output2,"Hello World!","rustc output is not true!")
    def tearDown(self):
        super().tearDown()
        self.cmd('yum remove git rust -y')
        self.cmd('rm -rf anolis-sys-tests-res helloworld*')