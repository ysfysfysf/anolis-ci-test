#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_shadow_umask_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class UmaskTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_user_permit(self):
        output = self.cmd('umask')
        self.assertEqual(output,'0022',"Default configuration is wrong!")
        self.cmd('rm -rf README.txt',ignore_status=True)
        self.cmd('touch README.txt')
        output1 = self.cmd('stat -c %a README.txt')
        self.assertEqual(output1,'644',"Default permission exception for new file!")
        self.cmd('chmod 744 README.txt')
        output2 = self.cmd('stat -c %a README.txt')
        self.assertEqual(output2,'744',"Permission exception of non file owner!")
    def tearDown(self):
        super().tearDown()
        self.cmd('rm -rf README.txt',ignore_status=True)