#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_yum_service_repo.py
@Time:      2022/8/31 10:06:45
@Author:    liuyaqing
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class YumRepoTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_repo_enable(self):
        curl_err_list = []
        output = self.cmd('cat /etc/yum.repos.d/* | grep baseurl | grep -v "#"')
        output1 = output.replace('$releasever',self.image.version).replace('$basearch',self.image.arch)
        self.log.debug(output1)
        for line in output1.split('\n'):
            cmd = line.split('://')[1]
            curl_cmd = 'curl -l -m 10 -o /dev/null -s -w %{http_code} '+cmd
            self.log.debug(curl_cmd)
            returncode = self.cmd(curl_cmd)
            # The return code of curl can not be 4xx or 5xx
            if returncode[0] is '4' or  returncode[0] is '5':
                curl_err_list.append(line)
        self.assertEqual(len(curl_err_list),0,'The inaccessable url: {}'.format(curl_err_list))