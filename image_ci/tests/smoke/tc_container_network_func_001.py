#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_network_func_001.py
@Time:      2022/9/28 11:06:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
import time
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ContainNetTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,app_container
    """

    def test_container_network(self):
        self.container_flag = 0
        output = self.cmd(self.container_engine+' inspect '+self.container_id+' | grep \"IPAddress\"  | awk -F ":" \'{print$2}\'')
        container_ip = output.split('"')[1]
        self.assertTrue(container_ip is not None,"No available ip!")

        output1 = self.cmd(self.container_engine+' inspect '+self.container_id+' | grep tcp | awk -F "/" \'{print$1}\'')
        if output1:
            container_port = output1.split('"')[1].strip()
        else:
            self.skip('Network related cases are not applicable to this image')       

        for i in range(4):
            time.sleep(5)
            self.cmd('timeout 5 telnet '+container_ip+' '+container_port+' > 1.txt',ignore_status=True)
            ret = self.cmd('cat 1.txt | grep Escape',ignore_status=True)
            if ret:
                break
        self.cmd('rm -rf 1.txt',ignore_status=True)
        self.assertTrue('Escape character is' in ret,"Container port not available")