#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_systemd_timezone_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class TimezoneTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """

    def test_timezone(self):
        output = self.cmd('ls /etc/localtime -l')
        self.assertEqual(output.split('/')[-1],'UTC',"timezone is wrong!")