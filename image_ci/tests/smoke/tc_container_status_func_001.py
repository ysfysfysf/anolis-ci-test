#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_status_func_001.py
@Time:      2022/9/22 19:56:45
@Author:    yangshunfeng
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ContainStatusTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,app_container
    """

    def test_container_basic(self):
        self.container_flag = 0
        container_status = self.cmd(self.container_engine+' inspect '+self.container_id+' | grep Status')
        self.assertTrue('running' in container_status,"The status of the application container is abnormal")

        container_run = self.cmd(self.container_engine+' inspect '+self.container_id+' | grep Running')
        self.assertTrue('true' in container_run,"The application container is not running")