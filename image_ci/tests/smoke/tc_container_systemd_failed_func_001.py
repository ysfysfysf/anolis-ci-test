#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_systemd_failed_func_001.py
@Time:      2022/8/31 10:06:45
@Author:    yangshunfeng.ysf
@Version:   1.0
@Contact:   lyq01395646@alibaba-inc.com
@License:   Mulan PSL v2
@Modified:  liuyaqing
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ServiceTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,baseos_container
    """
    def test_failed_service(self):
            fail_service = self.cmd('systemctl --failed | grep failed',ignore_status=True)
            failed_services = [line.split(' ')[1]
            for line in fail_service.split('\n') if line]
            self.assertEqual(len(failed_services), 0, 'The failed system services: {}'.format(' '.join(failed_services)))