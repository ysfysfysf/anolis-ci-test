#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_sysbench_keentune_001.py
@Time:      2023/3/3 11:06:45
@Author:    jiaominchao
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ContainSysbenchTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,keentune
    """
    def test_sysbench_status(self):
        self.container_flag = 1
        output = self.cmd('whereis -b sysbench')
        self.assertTrue('/sysbench' in output, "The status of the application container is abnormal")

