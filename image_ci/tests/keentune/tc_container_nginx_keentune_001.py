#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_nginx_keentune_001.py
@Time:      2023/3/3 11:06:45
@Author:    jiaominchao
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ContainNginxTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,keentune
    """
    def test_nginx_status(self):
        self.container_flag = 1
        self.cmd('/bin/bash nginx_keentune.sh')

        output = self.cmd('systemctl status nginx')
        self.assertTrue('running' in output, "The status of the application container is abnormal")

