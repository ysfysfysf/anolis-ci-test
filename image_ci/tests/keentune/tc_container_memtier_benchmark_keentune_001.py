#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File:      tc_container_memtier_benchmark_keentune_001.py
@Time:      2023/3/3 11:06:45
@Author:    jiaominchao
@Version:   1.0
@License:   Mulan PSL v2
"""
from common.basetest import BaseTest
from common.containertest import ContainerTest

class ContainMemTest(ContainerTest):
    """
    :avocado: tags=P1,noarch,local,keentune
    """
    def test_mem_status(self):
        self.container_flag = 1
        output = self.cmd('whereis -b memtier_benchmark')
        self.assertTrue('/memtier_benchmark' in output, "The status of the application container is abnormal")

