import sys
import time
from common.basetest import BaseTest, OSType


class ContainerTest(BaseTest):

    def setUp(self):
        super().setUp()
        if self.params.get('engine') is not None:
            self.container_engine = self.params.get('engine')
            self.create_container()

    def tearDown(self):
        super().tearDown()
        if self.params.get('engine') is not None:
            self.destroy_container()
    
    def create_container(self):
        database_dict = {'mysql': 'MYSQL_ROOT_PASSWORD', 'postgres': 'POSTGRES_PASSWORD', 'mariadb': 'MARIADB_ROOT_PASSWORD'}
        keentune_list = ['nginx_keentune', 'mysql_keentune', 'redis_keentune']
        if self.container_engine == 'docker':
            if self.image.ostype == OSType.ANOLIS:
                self.cmd('yum install -y yum-utils')
                self.cmd('yum install docker -y --enablerepo=Plus')
            elif self.image.ostype == OSType.CENTOS:
                self.cmd('yum install -y yum-utils')
                self.cmd('yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo')
                self.cmd('yum install docker-ce -y')
            elif self.image.ostype == OSType.FEDORA:
                self.cmd('dnf -y install dnf-plugins-core')
                self.cmd('dnf config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/fedora/docker-ce.repo')
                self.cmd('dnf update -y')
                self.cmd('dnf install docker-ce -y')
            elif self.image.ostype == OSType.OPENSUSE_LEAP:
                self.cmd('zypper install -y docker')
            elif self.image.ostype == OSType.UBUNTU:
                self.cmd('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
                self.cmd('add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"')
                self.cmd('apt-get update -y')
                if self.image.arch == 'aarch64':
                    self.cmd('rm -rf /etc/grub.d/10_linux*')
                    self.cmd('rm -rf /var/lib/dpkg/updates/*')
                self.cmd('DEBIAN_FRONTEND=noninteractive apt-get -y install docker-ce --no-install-recommends 1>/dev/null',ignore_status=True)
                output = self.cmd('service docker start',ignore_status=True)
                if 'docker.service is masked' in output:
                    self.cmd('systemctl unmask docker.service && systemctl unmask docker.socket')
                    self.cmd('systemctl start containerd.service')
            elif self.image.ostype == OSType.DEBIAN:
                self.cmd('apt-get update -y')
                self.cmd('apt-get install apt-transport-https ca-certificates curl gnupg software-properties-common -y 1>/dev/null')
                self.cmd('curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -')
                self.cmd('add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/debian $(lsb_release -cs) stable"')
                self.cmd('apt-get update -y')
                self.cmd('apt-get -y install docker-ce 1>/dev/null',ignore_status=True)
            else:
                self.log.debug("docker install: this operating system does not require testing")
                sys.exit(1)
            containerd_status = self.cmd('systemctl is-active containerd.service',ignore_status=True)
            if containerd_status != 'active':
                self.cmd('systemctl start containerd.service')
            for i in range(30):
                docker_status = self.cmd('systemctl is-active docker.service',ignore_status=True)
                if docker_status != 'active':
                    time.sleep(20)
                    self.cmd('systemctl reset-failed docker.service',ignore_status=True)
                    self.cmd('systemctl start docker.service',ignore_status=True)
                else:
                    break
        elif self.container_engine == 'podman':
            if self.image.ostype == OSType.ANOLIS or self.image.ostype == OSType.CENTOS:
                self.cmd('yum install -y podman')
            elif self.image.ostype == OSType.FEDORA:
                self.cmd('dnf install -y podman')
            elif self.image.ostype == OSType.OPENSUSE_LEAP:
                self.cmd('zypper install -y podman')
            elif self.image.ostype == OSType.UBUNTU:
                self.cmd('echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_'+self.image.version+'/ /" \
                | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list')
                self.cmd('curl -L "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_'+self.image.version+'/Release.key" \
                | sudo apt-key add -')
                self.cmd('apt-get update -y')
                self.cmd('DEBIAN_FRONTEND=noninteractive apt-get -y install podman --no-install-recommends')
            elif self.image.ostype == OSType.DEBIAN:
                self.cmd('apt-get update -y')
                self.cmd('apt-get install -y podman')
                registries = self.cmd('cat /etc/containers/registries.conf | grep docker.io',ignore_status=True)
                if '"docker.io"' not in registries:
                    process.run("touch my.conf")
                    process.run("echo unqualified-search-registries = [\"docker.io\"] > my.conf")
                    self.pwd = os.path.dirname(__file__)
                    self.destination = 'root@'+self.host+':/etc/containers'
                    source_file_c = self.pwd+'/../../my.conf'
                    self.session.copy_files(source_file_c,self.destination)
                    process.run("rm -rf my.conf",ignore_status=True)
                    self.cmd('cat /etc/containers/registries.conf /etc/containers/my.conf > /etc/containers/registries.conf')
            else:
                self.log.debug("podman install: this operating system does not require testing")
                sys.exit(1)
        else:
            self.log.debug("error container engine")
            sys.exit(1)
        for i in range(5):
            pull_log = self.cmd(self.container_engine+' pull '+self.version, ignore_status=True)
            if 'Pull complete' in pull_log:
                break
        output1 = self.cmd(self.container_engine+' image ls')
        if ':' in self.version:
            self.image_name = self.version.split(':')[0]
        else:
            self.image_name = self.version
        self.assertTrue(self.image_name in output1,"pull image failed")
        label = self.cmd(self.container_engine+' image inspect '+self.version+' | grep org.opencontainers.image.title')
        if label.split('"')[3] in list(database_dict.keys()):
            self.container_id = self.cmd(self.container_engine+' run -dit --privileged=true -e '+database_dict[label.split('"')[3]]+'=Hello123. '+self.version)
        elif label.split('"')[3] in keentune_list:
            self.container_id = self.cmd(self.container_engine+' run -dit --privileged=true '+self.version+' /usr/sbin/init ')
        else:
            self.container_id = self.cmd(self.container_engine+' run -dit --privileged=true '+self.version)
        container_status = self.cmd(self.container_engine+' ps -a | grep Up -wo')
        self.assertTrue('Up' in container_status,"run image failed")
        self.container_flag = 1

    def destroy_container(self):
        self.container_flag = 0
        if self.container_id:
            self.cmd(self.container_engine+' stop '+self.container_id)
            self.cmd(self.container_engine+' container prune -f')
            self.cmd(self.container_engine+' image rm '+self.version)
        if self.container_engine == 'docker':
            if self.image.ostype == OSType.ANOLIS:
                self.cmd('yum remove -y docker')
            elif self.image.ostype == OSType.CENTOS:
                self.cmd('yum remove docker-ce -y')
            elif self.image.ostype == OSType.FEDORA:
                self.cmd('dnf remove -y docker-ce')
            elif self.image.ostype == OSType.OPENSUSE_LEAP:
                self.cmd('zypper remove -y docker')
            elif self.image.ostype == OSType.UBUNTU or self.image.ostype == OSType.DEBIAN:
                self.cmd('apt-get -y remove docker-ce')
        elif self.container_engine == 'podman':
            if self.image.ostype == OSType.ANOLIS or self.image.ostype == OSType.CENTOS:
                self.cmd('yum remove -y podman')
            elif self.image.ostype == OSType.FEDORA:
                self.cmd('dnf remove -y podman')
            elif self.image.ostype == OSType.OPENSUSE_LEAP:
                self.cmd('zypper remove -y podman')
            elif self.image.ostype == OSType.UBUNTU:
                self.cmd('apt-get -y remove podman')
            elif self.image.ostype == OSType.DEBIAN:
                self.cmd('apt-get -y remove podman')
                self.cmd('rm -rf /etc/containers/my.conf',ignore_status=True)
