import os
from enum import Enum
from avocado import Test
from avocado.utils import process
from avocado.core import exceptions

class BaseTest(Test):

    def setUp(self):
        self.log.info('setting up...')
        self.container_flag = 0
        self.container_id = None
        self.host = self.params.get('host')
        self.version = self.params.get('version')
        self.image = self._get_image_info()

    def cmd(self, command, ignore_status=False):
        """
        Not support for awk remote command temporarily
        """
        if self.container_flag:
            command = self.container_engine+' exec '+self.container_id+' '+command

        if self.host and self.host == 'localhost':
            result = process.run(command, ignore_status=ignore_status,shell=True)
        else:
            self.fail('The image test does not support this test mode!')
        self.log.debug('command "{}" stdout: {}; exit code: {}'.format(
            command, result.stdout_text, result.exit_status))

        if result.exit_status != 0:
            if ignore_status:
                return result.stderr_text.strip()
            else:
                self.fail('failed to execute command "{}" with exit code {} and stderr {}'.format(
                    command, result.exit_status, result.stderr_text))
        return result.stdout_text.strip()

    def _get_image_info(self):
        image = Image()
        output = self.cmd('cat /etc/image-id | grep image_id')
        image.id = output.split('=')[1].strip('"')
        output = self.cmd('cat /etc/os-release')
        for line in output.split('\n'):
            if line.startswith('ID='):
                image.ostype = OSType[line.split('=')[1].strip('"').replace('-','_').upper()]
            elif line.startswith('VERSION_ID='):
                image.version = line.split('=')[1].strip('"')
        image.arch = self.cmd('arch')
        image.kernel = self.cmd('uname -r')
        self.log.debug('image.ostype: {}, image.version: {}, image.id: {}, image.arch: {}, image.kernel: {}'.format(
            image.ostype, image.version, image.id, image.arch, image.kernel))
        return image

    def skip(self, message=None):
        raise exceptions.TestSkipError(message)

class OSType(Enum):
    ANOLIS = 'anolis'
    CENTOS = 'centos'
    FEDORA = 'fedora'
    UBUNTU = 'ubuntu'
    OPENSUSE_LEAP = 'opensuse-leap'
    DEBIAN = 'debian'


class Image(object):

    def __init__(self):
        self.id = None
        self.arch = None
        self.kernel = None
        self.ostype = None
        self.version = None