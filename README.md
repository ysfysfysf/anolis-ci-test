# anolis-ci-test

#### 介绍
Anolis OS Packages CI test cases.


#### 使用说明
```
Usage:
	export PKG_CI_REPO_URL=<git_repo_url>
	export PKG_CI_REPO_BRANCH=<git_branch>
	export PKG_CI_PR_ID=<pull_request_id>

	./run_package_citest.sh [-h] [-t testcase]

Options:
	-h: print help info
	-t testcase: specify test case to run, valid test cases:
		check_license
		check_specfile
		check_codestyle
```

#### 参与贡献

1.  Fork本仓库
2.  新建分支
3.  提交代码
4.  新建 Pull Request

