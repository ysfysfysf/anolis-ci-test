#!/bin/sh
# Usage:
# export PKG_CI_REPO_URL=<git_repo_url>
# export PKG_CI_REPO_BRANCH=<git_branch>
# export PKG_CI_PR_ID=<pull_request_id>
#
# Notes:
# PKG_CI_REPO_URL is url of the target git repo
# PKG_CI_REPO_BRANCH is branch of the target git repo
#
# #Get help list:
# ./run_package_citest.sh -h
# #Run a single test:
# ./run_package_citest.sh -t check_license
# #Run several tests:
# ./run_package_citest.sh -t check_license -t check_specfile

. ./lib/lib.sh

[ -n "$1" ] || { usage; exit 0; }

# parse args
test_list=""
while getopts "ht:" opt; do
	case ${opt} in
		t)
			test_list="$test_list $OPTARG"
			;;
		h)
			usage && exit 0
			;;
		\?)
			echo "Invalid option: $OPTARG" 1>&2
			;;
	esac
done
shift $((OPTIND -1))

[ -n "$test_list" ] || die "No test case is specified, use '$0 -t casename'"

# main process
[ -n "$PKG_CI_REPO_URL" ] || die "PKG_CI_REPO_URL is required but not set!"
[ -n "$PKG_CI_REPO_BRANCH" ] || die "PKG_CI_REPO_BRANCH is required but not set!"
[ -n "$PKG_CI_PR_ID" ] || die "PKG_CI_PR_ID is required but not set!"

work_dir=$(readlink -f work_dir)
[ -d "$work_dir" ] || mkdir $work_dir
util_dir=$(readlink -f utils)

pkg_name=$(basename $PKG_CI_REPO_URL .git)
pkg_path="$work_dir/$pkg_name"
pr_branch="pull-request-${PKG_CI_PR_ID}"

ret=0
os_version=$(cat /etc/os-release | grep ID= |awk -F'"' '{print $2}'|xargs)
if echo $os_version | grep "anolis 23"; then
	creat_anolis23_build_repo
fi
for t in $test_list; do
	echo "====Run Test: $t"
	$t
done

exit $ret
