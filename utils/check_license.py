#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import sys
import argparse
from license_check.license_check import LicenseCheck

# parse commandline options
parser = argparse.ArgumentParser(description='Check package license in spec file or source dir')
parser.add_argument('--specfile', type=str, help='spec file')
parser.add_argument('--sourcedir', type=str, help='source dir')
args = parser.parse_args()

# check licenses in rpm spec or project source
# ret: 0 - pass, >=1 - fail; -1 - warning
ret = 0
lc = LicenseCheck()
if args.specfile:
    ret = lc.check_licenses_in_spec(args.specfile)
    #if args.sourcedir:
    #    ret2 = lc.compare_licenses_in_spec_and_src(args.specfile, args.sourcedir)
    #    ret += ret2
elif args.sourcedir and not args.specfile:
    ret = lc.check_licenses_in_src(args.sourcedir)
else:
    print("Specify specfile or sourcedir or both, use '-h' for help")

exit(ret)
