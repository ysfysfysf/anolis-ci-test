#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import yaml
import shutil
import subprocess
from openpyxl import load_workbook


class LicenseCheck(object):
    def __init__(self):
        self.work_dir = os.path.dirname(os.path.abspath(__file__))

        self.licenses_matrix = {}
        self.licenses_matrix.setdefault('all', {})
        self.licenses_matrix.setdefault('white', [])
        self.licenses_matrix.setdefault('black', [])
        self._load_white_black_list()

        self.spdx_license_web = "https://spdx.org/licenses/"
        self.spdx_license_list = []
        self._load_spdx_license_list()

        self.licenses_spdx_adapt = {}
        self._load_spdx_adaptation()
        self._update_white_black_list()

        self.spec_lic_loaded = False
        self.licenses_in_spec = {}
        self.licenses_in_spec.setdefault('rpms', {})
        self.licenses_in_spec.setdefault('all', [])
        self.licenses_in_spec.setdefault('white', [])
        self.licenses_in_spec.setdefault('black', [])
        self.licenses_in_spec.setdefault('unknown', [])
        self.licenses_in_spec.setdefault('checked', [])

        self.src_lic_loaded = False
        self.licenses_in_src = {}
        self.licenses_in_src.setdefault('all', [])
        self.licenses_in_src.setdefault('white', [])
        self.licenses_in_src.setdefault('black', [])
        self.licenses_in_src.setdefault('unknown', [])

    def _load_white_black_list(self):
        white_black_list = os.path.join(self.work_dir, 'white_black.xlsx')
        if not os.path.exists(white_black_list):
            print("No such file: %s" % white_black_list)
            exit(1)

        wb = load_workbook(white_black_list, read_only=True)
        ws_white = wb.get_sheet_by_name('许可证白名单')
        ws_black = wb.get_sheet_by_name('许可证黑名单')
        white_licenses = self._get_licenses_from_sheet(ws_white)
        black_licenses = self._get_licenses_from_sheet(ws_black)
        wb.close()

        self.licenses_matrix['all'].update(white_licenses)
        self.licenses_matrix['all'].update(black_licenses)
        # save license full names and nick names both in list
        for lic in white_licenses:
            lic_arr = [lic]
            lic_arr.extend(white_licenses[lic]['alias'])
            for x in lic_arr:
                if x not in self.licenses_matrix['white']:
                    self.licenses_matrix['white'].append(x)
        for lic in black_licenses:
            lic_arr = [lic]
            lic_arr.extend(black_licenses[lic]['alias'])
            for x in lic_arr:
                if x not in self.licenses_matrix['black']:
                    self.licenses_matrix['black'].append(x)

    def _load_spdx_license_list(self):
        """Download latest spdx licenses.md or use saved one"""
        spdx_license_list_new = os.path.join(self.work_dir, 'spdx-license-list.md')
        spdx_license_list_old = os.path.join(self.work_dir, 'spdx-license-list-save.md')
        spdx_license_list_url = 'https://raw.githubusercontent.com/spdx/license-list-data/master/licenses.md'
        cmd = 'curl -o {} {}'.format(spdx_license_list_new, spdx_license_list_url)
        try:
            ret = subprocess.call(cmd, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, timeout=5)
        except:
            ret = 1
        if ret == 1:
            try:
                shutil.copy(spdx_license_list_old, spdx_license_list_new)
            except IOError as e:
                print("Unable to copy file. %s" % e)
                return 1
            except:
                print("Unexpected error:", sys.exc_info())
                return 1
        with open(spdx_license_list_new, 'r') as f:
            for line in f:
                m = re.match(r'^\[(.*)\]: .*\.txt$', line)
                if m:
                    self.spdx_license_list.append(m.group(1))

    def _get_licenses_from_sheet(self, work_sheet):
        licenses = {}
        rows = work_sheet.values
        header = next(rows)
        for row in rows:
            full_name = row[0]
            short_name = row[1]
            if full_name == None:
                continue
            if short_name == None:
                short_name = full_name
            if full_name not in licenses:
                licenses[full_name] = {'name': full_name, 'alias': [short_name]}
        return licenses
     
    def _load_spdx_adaptation(self):
        licenses_spdx_yaml = os.path.join(self.work_dir, 'license_alias.yaml')
        with open(licenses_spdx_yaml, 'r') as f:
            try:
                self.licenses_spdx_adapt = yaml.load(f)
            except(TypeError):
                self.licenses_spdx_adapt = yaml.load(f, Loader=yaml.FullLoader)

    def _update_white_black_list(self):
        """Add old license alias to while/black list, e.g. GPLv2 = GPL-2.0"""
        if not self.licenses_spdx_adapt:
            return
        tmp_white_lics = []
        for lic in self.licenses_matrix['white']:
            tmp_white_lics.extend(self.licenses_spdx_adapt.get(lic,[]))
        for lic in tmp_white_lics:
            if lic not in self.licenses_matrix['white']:
                self.licenses_matrix['white'].append(lic)

        tmp_black_lics = []
        for lic in self.licenses_matrix['black']:
            tmp_black_lics.extend(self.licenses_spdx_adapt.get(lic,[]))
        for lic in tmp_black_lics:
            if lic not in self.licenses_matrix['black']:
                self.licenses_matrix['black'].append(lic)

    def get_licenses_from_spec(self, specfile):
        """Get licenses from spec file, use rpmspec or read specfile directly"""
        print("Read licenses from spec file: %s" % specfile)
        tags = ['NAME','VERSION','LICENSE']
        queryformat = '|'.join(['%{'+i+'}' for i in tags])
        outputfile = '/tmp/tmp-license-list.txt'
        cmd = 'rpmspec -q --qf \"|{}|\\n\" {} > {}'.format(queryformat, specfile, outputfile)
        try:
            ret = subprocess.call(cmd, shell=True, stderr=subprocess.DEVNULL)
        except:
            ret = 1

        if ret == 0:
            with open(outputfile, 'r') as f:
                for line in f:
                    if not line.startswith('|'):
                        continue
                    arr = line.split('|')
                    name = arr[1]
                    lics = arr[3]
                    if name not in self.licenses_in_spec['rpms']:
                        self.licenses_in_spec['rpms'][name] = lics
        else:
            with open(specfile, 'r') as f:
                for line in f:
                        m = re.match(r'^Name:(\s+)(.*)', line)
                        if m:
                            name = m.group(2)
                            continue

                        m = re.match(r'^License:(\s+)(.*)', line)
                        if m:
                            lics = m.group(2)
                            break
            if name not in self.licenses_in_spec['rpms']:
                self.licenses_in_spec['rpms'][name] = lics

        if len(self.licenses_in_spec['rpms']) < 1:
            print("Error: Failed to get licenses from spec file")
            return 1

        self.spec_lic_loaded = True
        return 0

    def check_licenses_in_spec(self, specfile):
        if not self.spec_lic_loaded:
            self.get_licenses_from_spec(specfile)
        if not self.licenses_in_spec.get('rpms'):
            return 1

        cnt_fail = 0
        cnt_warn = 0
        for rpm in self.licenses_in_spec.get('rpms'):
            ret = self.check_rpm_licenses(rpm)
            if ret > 0:
                cnt_fail += 1
            elif ret == -1:
                cnt_warn += 1

        # The license name should conform to SPDX standard
        if self.licenses_in_spec.get('all'):
            ret = self.check_spdx_spec(self.licenses_in_spec['all'])
            if ret:
                cnt_warn += 1

        if cnt_fail > 0:
            print("Failed: Licenses of some rpm packages are unsafe!")
            return 1
        elif cnt_warn > 0:
            print("Warning: Licenses of some rpm packages need confirm!")
            return -1
        else:
            print("Passed: Licenses of all rpm packages are safe!")
            return 0

    def check_rpm_licenses(self, name):
        lics = self.licenses_in_spec['rpms'][name]
        if not lics:
            print("Not find licenses of rpm: %s" % name)
            return 0
        if lics in self.licenses_in_spec['checked']:
            print("Same licenses have been checked %s: %s" % (name, lics))
            return 0

        print("\nCheck licenses of %s: %s" % (name, lics))
        self.licenses_in_spec['checked'].append(lics)
        # lics: (GPLv2 or GPLv3) and GPLv2+ with exceptions and LGPLv2+ with exceptions and MIT
        # lics_arr0: [(GPLv2, or, GPLv3), and, GPLv2+, with, exceptions, and, LGPLv2+, with, exceptions, and, MIT]
        # lics_arr1: [(GPLv2, GPLv3), GPLv2+ with exceptions, LGPLv2+ with exceptions, MIT]
        # lics_arr2: [or, and, and, and]
        lics_arr0 = lics.split()
        lics_arr1 = []
        lics_arr2 = []
        tmp = ''
        for word in lics_arr0:
            if word.lower() == 'and' or word.lower() == 'or':
                if tmp:
                    lics_arr1.append(tmp)
                    tmp = ''
                lics_arr2.append(word.lower())
            else:
                if tmp:
                    tmp = tmp + ' ' + word
                else:
                    tmp = word
        if tmp:
            lics_arr1.append(tmp)
        lics_arr2.append('')

        cnt_pass = 0
        cnt_fail = 0
        cnt_warn = 0
        lics_arr1_1 = []
        for word in lics_arr1:
            lic = word.strip('()')
            lic_s = re.sub('\s+with.*exceptions', '', lic)
            lic_s = re.sub('Dual-licesed\s+', '', lic_s)

            if lic_s not in self.licenses_in_spec['all']:
                self.licenses_in_spec['all'].append(lic_s)

            if lic_s in self.licenses_matrix['white']:
                print("License: %-10s -- white" % lic)
                if lic_s not in self.licenses_in_spec['white']:
                    self.licenses_in_spec['white'].append(lic_s)
                lics_arr1_1.append(word.replace(lic, '1'))
            elif lic_s in self.licenses_matrix['black']:
                cnt_warn += 1
                print("License: %-10s -- black" % lic)
                if lic_s not in self.licenses_in_spec['black']:
                    self.licenses_in_spec['black'].append(lic_s)
                lics_arr1_1.append(word.replace(lic, '0'))
            else:
                cnt_warn += 1
                print("License: %-10s -- unknown" % lic)
                if lic_s not in self.licenses_in_spec['unknown']:
                    self.licenses_in_spec['unknown'].append(lic_s)
                lics_arr1_1.append(word.replace(lic, '0'))
        condition = ' '.join(v + ' ' + d for v,d in zip(lics_arr1_1, lics_arr2)).strip()
        try:
            res = eval(condition)
            if res == 1:
                print("Check condition passed: (%s) = 1" % condition)
            else:
                cnt_fail += 1
                print("Check condition failed: (%s) = 0" % condition)
        except:
            print("Invalid condition: %s" % condition)
            return -1

        if cnt_fail > 0:
            print("Error: failed to check licenses of %s\n" % name)
            return 1
        if cnt_warn > 0:
            print("Warning: find black or unknown licenses in %s\n" % name)
            return -1

        print("Succeed to check licenses of %s\n" % name)
        return 0

    def check_spdx_spec(self, licenses):
        """Check the SPDX licenses list"""
        cnt_fail = 0
        for lic in licenses:
            if lic not in self.spdx_license_list:
                cnt_fail += 1
                print("Warning: License name %s is not conform to SPDX standard, please check %s" % (lic, self.spdx_license_web))
        return cnt_fail

    def _get_licenses_files(self, source_dir):
        """Get the supported license files"""
        license_files = []

        supported_lic_file_name = []
        with open(os.path.join(self.work_dir, "license_files.txt")) as fl:
            supported_lic_files = fl.read().split("\n")
            for fname in supported_lic_files:
                if fname.strip().upper() not in supported_lic_file_name:
                    supported_lic_file_name.append(fname.strip().upper())
        if not supported_lic_file_name:
            print("license_files.txt may NOT include any content")
            return []

        for name in os.listdir(source_dir):
            _current = os.path.join(source_dir, name)
            # todo: license in a directory
            if name.upper() in supported_lic_file_name:
                license_files.append(_current)
        return license_files

    def get_licenses_from_src(self, sourcedir):
        """
        The licenses read by spdx match tool are SPDX Identifier.
        """
        print("Read licenses from source dir...")
        license_files = self._get_licenses_files(os.path.join(self.work_dir, sourcedir))
        if not license_files:
            print("Failed to get licenses from source dir")
            return False

        arch = os.popen("arch").read().strip()
        for i in license_files:
            result = os.popen("{}-{} id {}".format(os.path.join(self.work_dir, "askalono"), arch, i)).read()
            lic = result.split()[1]
            if lic not in self.licenses_in_src["all"]:
                self.licenses_in_src["all"].append(lic)

    def check_licenses_in_src(self, sourcedir):
        if not self.src_lic_loaded:
            self.get_licenses_from_src(sourcedir)

        if not self.licenses_in_src.get('all'):
            return 1

        print("Check licenses in source dir: %s" % 
            ','.join(self.licenses_in_src['all']))
        cnt_black = 0
        cnt_unknown = 0
        for lic in self.licenses_in_src.get('all'):
            lic_alias = []
            lic_alias.append(lic)
            if lic in self.licenses_spdx_adapt:
                lic_alias.extend(self.licenses_spdx_adapt[lic])

            hit = 0
            for tag in lic_alias:
                if tag in self.licenses_matrix.get('white'):
                    self.licenses_in_src['white'].append(lic)
                    print("License: %-10s -- white" % lic)
                    hit = 1
                    break
                elif tag in self.licenses_matrix.get('black'):
                    self.licenses_in_src['black'].append(lic)
                    print("License: %-10s -- black" % lic)
                    cnt_black += 1
                    hit = 1
                    break

            if hit == 0:
                self.licenses_in_src['unknown'].append(lic)
                print("License: %-10s -- unknown" % lic)
                cnt_unknown += 1

        ret = cnt_black + cnt_unknown
        if ret == 0:
            print("All licenses in source dir are safe (in white list).")
        else:
            if cnt_black > 0: 
                print("Error: Some licenses in source dir are unsafe (in black list).")
            if cnt_unknown > 0:
                print("Error: Some licenses in source dir need confirm (unknown type).")
        return ret

    def compare_licenses_in_spec_and_src(self, specfile, sourcedir):
        if not self.spec_lic_loaded:
            self.get_licenses_from_spec(specfile)
        if not self.src_lic_loaded:
            self.get_licenses_from_src(sourcedir)

        if not self.licenses_in_spec.get('all') and \
            not self.licenses_in_src.get('all'):
            print("No License found in spec file and source tree.")
            return 1
        if self.licenses_in_spec.get('all') and \
            not self.licenses_in_src.get('all'):
            print("No License found in source tree but in spec file, no need to compare.")
            return 0
        
        print("Check licenses in spec file and source dir are same...") 
        print("Licenses in spec file: %s" % self.licenses_in_spec.get('raw'))
        print("Licenses in source dir: %s" % 
            ','.join(self.licenses_in_src['all']))
        licenses_in_spec = ','.join(self.licenses_in_spec.get('all'))

        ret = 0
        for lic in self.licenses_in_src.get('all'):
            lic_alias = []
            lic_alias.append(lic)
            if lic in self.licenses_spdx_adapt:
                lic_alias.extend(self.licenses_spdx_adapt[lic])

            hit = 0
            for tag in lic_alias:
                if licenses_in_spec.find(tag) >= 0:
                    hit = 1
                    break

            if hit == 0:
                print("Error: %s is not found in spec file" % lic)
                ret += 1
        
        if ret == 0:
            print("All licenses in source dir have been included in spec file.")

        return ret
