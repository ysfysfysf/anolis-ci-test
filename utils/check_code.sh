#!/bin/sh
export LANG=C

[ -n "$1" ] || { echo "$0 file_to_check"; exit 1; }
file_to_check=$1
util_dir=$(readlink -f $(dirname $0))

. $util_dir/../lib/lib.sh

is_c_cplusplus()
{
	basename $file_to_check | grep -qE "*\.c$|*\.cpp$|*\.cc$" || file $file_to_check | grep -qE "C source|C\+\+ source"
}

check_c_cplusplus()
{
	which cppcheck &>/dev/null || \
		{ echo "cppcheck is not found, skip"; return; }
	cppcheck --error-exitcode=22 $file_to_check
}

is_shell_script()
{
	basename $file_to_check | grep -qE "*\.sh$" || file $file_to_check | grep -qE "shell script"
}

check_shell_script()
{
	which shellcheck &>/dev/null || \
		{ echo "shellcheck is not found, skip"; return; }
	shellcheck -S error $file_to_check
}

is_python_script()
{
	basename $file_to_check | grep -qE "*\.py$" || file $file_to_check | grep -qE " Python script"
}

check_python_script()
{
	which pylint &>/dev/null || \
		{ echo "pylint is not found, skip"; return; }
	pylint --rcfile=$util_dir/pylint.conf $file_to_check
}

# Install cppcheck
which cppcheck &>/dev/null || yum_install cppcheck

# Install shellcheck
which shellcheck &>/dev/null || {
	tar xf $util_dir/code_check/shellcheck-stable.linux.x86_64.tar.xz -C /tmp
	cp /tmp/shellcheck-stable/shellcheck /usr/local/bin/
}

# Install pylint
which pylint &>/dev/null || {
	which pip3 &>/dev/null || yum_install python3-pip
	pip_install pylint
	pylint --persistent=n --generate-rcfile > $util_dir/pylint.conf
}

echo "====Check code of $file_to_check"
[ -f "$file_to_check" ] || { echo "No such file: $file_to_check"; exit 1; }

ret=0
if is_c_cplusplus $file_to_check; then
	check_c_cplusplus $file_to_check
	[ $? -eq 22 ] && ret=1
elif is_shell_script $file_to_check; then
	check_shell_script $file_to_check
	[ $? -ne 0 ] && ret=1
elif is_python_script $file_to_check; then
	check_python_script $file_to_check
	[ $? -ne 0 ] && ret=1
else
	echo "No checker is found, skip"
fi

[ "$ret" -ne 0 ] && exit 1
exit 0
