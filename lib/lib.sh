#!/bin/sh
BIN_FILE_LIST="/tmp/binfile.list"
SERVICE_ACTION=("stop" "start" "restart" "status")
PKG_CI_TEST_PATH="/tmp/pkg_ci_test"
RPM_DIR_OLD="/tmp/pkg_ci_test_old"
RPM_PKG_LIST="/tmp/pkglist"
# "check-sat" "check-repoclosure" "check-conflicts" "check-upgrade",now check repoclosure only
RPMDEPLINT_PARAM=("check-repoclosure")
declare -A PKG_INSTALL_HASH
declare -A PKG_HASH_OLD
declare -A PKG_HASH_NEW

usage()
{
	cat <<EOF
Usage:
	export PKG_CI_REPO_URL=<git_repo_url>
	export PKG_CI_REPO_BRANCH=<git_branch>
	export PKG_CI_PR_ID=<pull_request_id>

	$0 [-h] [-t testcase]

Options:
	-h: print help info
	-t testcase: specify test case to run, valid test cases:
		check_license
		check_specfile
		check_codestyle
		pkg_smoke_test
		check_abi_diff
		check_pkg_dependency
EOF
}

log_cmd()
{
	echo "$@"
	eval "$@"
}

die()
{
	echo "$@"
	exit 1
}

pass()
{
	echo "====PASS: $*"
}

fail()
{
	echo "====FAIL: $*"
	ret=$((ret + 1))
}

skip()
{
	echo "====SKIP: $*"
}

warning()
{
	echo "====WARNING: $*"
}
yum_install()
{
	local ret
	pkgs="$*"
	tmplog=$(mktemp)
	yum -y install $pkgs --nogpgcheck &>$tmplog
	ret=$?
	if [ "$ret" -ne 0 ]; then
		echo "ERROR: yum install $pkgs"
		cat $tmplog
	fi
	rm -f $tmplog
	return $ret
}

pip_install()
{
	local ret
	pkgs="$*"
	tmplog=$(mktemp)
	pip3 install $pkgs &>$tmplog
	ret=$?
	if [ "$?" -ne 0 ]; then
		echo "ERROR: yum install $pkgs"
		cat $tmplog
	fi
	rm -f $tmplog
	return $ret
}

checkout_pkgsrc()
{
	# clone git repo only once
	[ -d "$pkg_path/.git" ] && return 0

	echo "Clone git repo: $PKG_CI_REPO_URL"
	cd `dirname $pkg_path`
	log_cmd "git clone -b $PKG_CI_REPO_BRANCH $PKG_CI_REPO_URL" || return 1

	echo "Fetch pull request: !$PKG_CI_PR_ID"
	cd $pkg_path
	git config remote.origin.fetch '+refs/pull/*:refs/remotes/origin/pull/*'
	log_cmd "git fetch origin pull/${PKG_CI_PR_ID}/MERGE:${pr_branch}" || return 1
	log_cmd "git checkout $pr_branch" || return 1

	if [ -f download ]; then
		if [ -s download ]; then
			download_source
		else
			echo "ERROR: download file is null."
			return 1
		fi
	fi
	return 0
}

download_source()
{
	url_base="http://build.openanolis.cn/kojifiles/upstream-source/"
	# check if download file is empty
	local cnt=$(grep -v ^# download | wc -l)
	if [ -n "$cnt" ] && [ "$cnt" -eq 0 ]; then
		echo "ERROR: download file is empty"
		return 1
	fi
	local ret=0
	while read line; do
		src_name=$(echo "$line" | awk '{print $2}')
		src_md5=$(echo "$line" | awk '{print $1}')
		src_url="${url_base}/${src_name}.${src_md5}"
		log_cmd curl -o $src_name $src_url || ret=$((ret+1))
	done < download
	return $ret
}

setup_rpmdev_tree()
{
	which rpmdev-setuptree &>/dev/null || yum_install rpmdevtools
	which rsync &>/dev/null || yum_install rsync
	which rpmbuild &>/dev/null || yum_install rpm-build
	which libtoolize &>/dev/null || yum_install libtool
	
	RPMBUILD="${HOME}/rpmbuild"
	[ -d "$RPMBUILD" ] || rpmdev-setuptree
	echo "export RPMBUILD=$RPMBUILD"
	export RPMBUILD=$RPMBUILD
	
	rsync -a --exclude=".git" --exclude="*.spec" $pkg_path/ $RPMBUILD/SOURCES/
	rsync -a $pkg_path/*.spec $RPMBUILD/SPECS/
	if ls $pkg_path/*.spec >/dev/null; then
		pkg_spec=$(basename $(ls $pkg_path/*.spec))
	else
		echo "No such file: $pkg_spec"
		return 1
	fi
	build_dir="${RPMBUILD}"/BUILD
	rm -rf "${build_dir}"/*
	tmplog=$(mktemp)
	rpmbuild -bp --nodeps --nocheck $pkg_path/$pkg_spec &>$tmplog
	[ "$?" -ne 0 ] && {
		cat $tmplog
		echo "Error: rpmbuild error."
		return 1
	}
	rm -f $tmplog
}

setup_python_env()
{
	which python3 &>/dev/null || yum_install python3
	which pip3 &>/dev/null || yum_install python3-pip
	rpm -qa | grep python3-openpyxl &>/dev/null || yum_install python3-openpyxl &>/dev/null || {
		pip_install openpyxl || return 1
	}
	rpm -qa | grep python3-pyyaml &>/dev/null || yum_install python3-pyyaml &>/dev/null || {
		pip_install pyyaml || return 1
	}
}

# Test case: check package license type
check_license()
{
	local ret
	checkout_pkgsrc || {
		fail "$FUNCNAME"
		die "ERROR: checkout pkg src failed."
	}
	setup_python_env || {
		fail "$FUNCNAME"
		die "ERROR: setup python env failed."
	}

	# for rpm tree, check the license in spec file,
	# otherwise, check the license txt in source dir
	if is_rpm_tree; then
		pkg_spec=$(basename $(ls $pkg_path/*.spec))
		cd $pkg_path
		grep -qE "^+License:" $pkg_spec ||{
			fail "$FUNCNAME"
			die "Failed to get licenses from spec file"
		}
		# pass if license is not changed
		git diff $PKG_CI_REPO_BRANCH $pr_branch -- *.spec | \
			grep -qE "^+License:" || {
			echo "License in spec is not changed"
			pass "$FUNCNAME"
			return 0
		}
		setup_rpmdev_tree
		tst_cmd="./check_license.py --specfile=$pkg_path/$pkg_spec"
	else
		tst_cmd="./check_license.py --sourcedir=$pkg_path"
	fi
	cd $util_dir
	log_cmd "$tst_cmd"
	ret=$?
	if [ "$ret" -eq 0 ]; then
		pass "$FUNCNAME"
	elif [ "$ret" -eq 255 ]; then
		warning "$FUNCNAME"
	else
		fail "$FUNCNAME"
	fi
}

# Test case: check package spec file
check_specfile()
{
	# check repo source url black list
	grep $PKG_CI_REPO_SOURCE_URL $util_dir/black_list/check_specfile.list && { skip "$FUNCNAME"; return 0; }
	# valid for rpm tree only
	is_rpm_tree || { skip "$FUNCNAME"; return 0; }
	which autoreconf &>/dev/null || yum_install autoconf
	mkdir -p $PKG_CI_TEST_PATH
	checkout_pkgsrc || {
		fail "$FUNCNAME"
		die "ERROR: checkout pkg src failed."
	}

	if ls $pkg_path/*.spec >/dev/null; then
		pkg_spec=$(basename $(ls $pkg_path/*.spec))
	else
		echo "Can't find spec file: $pkg_path"
		fail "$FUNCNAME"
		return 1
	fi
	spec_file="${pkg_path}/${pkg_spec}"

	old_rpm_list="${work_dir}/${pkg_name}.old_rpm_list"
	[ -f "$old_rpm_list" ] && rm -f $old_rpm_list
	new_rpm_list="${work_dir}/${pkg_name}.new_rpm_list"
	[ -f "$new_rpm_list" ] && rm -f $new_rpm_list
	which rpmspec &>/dev/null || yum_install rpm-build
	rpmspec="rpmspec -q --qf '%{NAME} %{VERSION} %{RELEASE}\n'"

	setup_rpmdev_tree
	# get new rpm list
	eval $rpmspec $spec_file > $new_rpm_list
	# get old rpm list
	cd $pkg_path
	pwd
	git checkout $PKG_CI_REPO_BRANCH
	[ -f "$spec_file" ] && \
	eval $rpmspec $spec_file > $old_rpm_list
	# back to pr branch
	git checkout $pr_branch ||{
		fail "$FUNCNAME"
		die "ERROR: checkout pr branch failed."
	}

	local ret=0
	check_rpm_names || ret=$(($ret + 1))
	check_rpm_version || ret=$(($ret + 1))

	git diff $PKG_CI_REPO_BRANCH $pr_branch | \
	grep "^diff --git" | grep -q "$pkg_spec" && {
		check_rpm_spec || ret=$(($ret + 1))
		cat $PKG_CI_TEST_PATH/check_spec.log 
	}
	if [ "$ret" -ne 0 ]; then
		warning "$FUNCNAME"
	else
		pass "$FUNCNAME"
	fi
	rm -rf $PKG_CI_TEST_PATH
}

#Check rpm names are not changed
#Input:  old_rpm_list new_rpm_list
#Output: 0 - pass; 1 - fail(changed)
check_rpm_names()
{
	# return pass if no old rpm
	[ -f "$old_rpm_list" ] || return 0
	# return fail if no new rpm
	[ -f "$new_rpm_list" ] || return 1

	local ret
	echo "Check rpm names are same"
	old_rpms=$(mktemp XXXXXX-oldrpms.txt)
	new_rpms=$(mktemp XXXXXX-newrpms.txt)
	awk '{print $1}' $old_rpm_list > $old_rpms
	awk '{print $1}' $new_rpm_list > $new_rpms

	which diff &>/dev/null || yum_install diffutils
	diff -Nu $old_rpms $new_rpms
	ret=$?
	[ "$ret" -ne 0 ] && echo "Warning: rpm names are changed"

	rm -f $old_rpms $new_rpms
	return $ret
}

#Check rpm version+release are changed
#Input:  old_rpm_list new_rpm_list
#Output: 0 - pass; 1 - fail(not changed)
check_rpm_version()
{
	# return pass if no old rpm
	[ -f "$old_rpm_list" ] || return 0
	# return fail if no new rpm
	[ -f "$new_rpm_list" ] || return 1

	echo "Check rpm release number is updated"
	local ret=0
	for pkg in $(awk '{print $1}' $new_rpm_list)
	do
		grep -q "${pkg} " $old_rpm_list || continue
		old_version=$(grep "${pkg} " $old_rpm_list | awk '{print $2}')
		[ -n "$old_version" ] || {
			echo "Can't get rpm old version of $pkg"
			continue
		}
		old_release=$(grep "${pkg} " $old_rpm_list | awk '{print $3}')
		[ -n "$old_release" ] || {
			echo "Can't get rpm old release of $pkg"
			continue
		}
		echo "[old] $pkg $old_version $old_release"
		new_version=$(grep "${pkg} " $new_rpm_list | awk '{print $2}')
		[ -n "$new_version" ] || {
			echo "Can't get rpm new version of $pkg"
			continue
		}
		new_release=$(grep "${pkg} " $new_rpm_list | awk '{print $3}')
		[ -n "$new_release" ] || {
			echo "Can't get rpm new release of $pkg"
			continue
		}
		echo "[new] $pkg $new_version $new_release"
		#pass if package version is changed
		[ "$new_version" != "$old_version" ] && continue
		#fail if release number is not changed
		[ "$new_release" == "$old_release" ] && {
			echo "Warning: rpm release of $pkg is not changed"
			ret=$(($ret + 1))
		}
	done
	[ "$ret" -eq 0 ] && echo "Pass: package version is udpated"
	return $ret
}

check_rpm_spec()
{
	which rpmlint &>/dev/null || yum_install rpmlint
	rpm -q glibc-langpack-en &>/dev/null || yum_install glibc-langpack-en
	export LC_ALL=en_US.UTF-8
	which checkbashisms &>/dev/null || yum_install devscripts-checkbashisms
	rpmlint $spec_file > $PKG_CI_TEST_PATH/check_spec.log
}

# Test case: check package code style of changed files
check_codestyle()
{
	local ret=0
	# check repo source url black list
	grep $PKG_CI_REPO_SOURCE_URL $util_dir/black_list/check_codestyle.list && { skip "$FUNCNAME"; return 0; }
	which automake &>/dev/null || yum_install automake || {
		fail "$FUNCNAME"
		die "Error: install automake failed."
	}
 	which autopoint &>/dev/null || yum_install gettext-devel || {
		fail "$FUNCNAME"
		die "Error: install gettext-devel failed."
	}
	mkdir -p $PKG_CI_TEST_PATH
	checkout_pkgsrc || {
		fail "$FUNCNAME"
		die "ERROR: checkout pkg src failed."
	}
	cd $work_dir
	get_diff_files || {
		fail "$FUNCNAME"
		die "Error: get diff files failed."
    }
	[ -f "$diff_files_to_check" ] || {
		skip "$FUNCNAME"
		echo "Info: no diff files to check."
		return 0
	}
	while read f
	do
		echo $f | \
		grep -qE "\.spec$|\.patch$|\.tar$|\.tar.gz$|\.tgz$|\.tar.bz2$|\.tar.xz$" \
		&& continue
		$util_dir/check_code.sh $f >> $PKG_CI_TEST_PATH/check_code.log 2>&1
		if [ "$?" -ne 0 ]; then
			echo "$util_dir/check_code.sh $f -- fail"
			ret=$((ret + 1))
		else
			echo "$util_dir/check_code.sh $f -- pass"
		fi
	done < $diff_files_to_check
	[ $ret -eq 0 ] && pass "$FUNCNAME" || {
		cat $PKG_CI_TEST_PATH/check_code.log
		warning "$FUNCNAME"
	}
	rm -rf $PKG_CI_TEST_PATH
}

get_diff_files()
{
	local file_name
	diff_files_in_tree="${work_dir}/${pkg_name}.diff_files_in_tree"
	cd $pkg_path
	git diff $PKG_CI_REPO_BRANCH $pr_branch | grep "^diff --git" | awk '{print $NF}' | sed 's#b/##g' > $diff_files_in_tree
	diff_files_to_check="${work_dir}/${pkg_name}.files_to_check"
	[ -f "$diff_files_to_check" ] && rm -f $diff_files_to_check
	[ -n "$build_dir" ] && src_dir=$build_dir || src_dir=$work_dir
	while read line
	do
		file_name=${line##*/}
		find $src_dir -name $file_name | grep $line >> $diff_files_to_check
	done < $diff_files_in_tree
	is_rpm_tree || return 0

	diff_files_in_patch="${work_dir}/${pkg_name}.diff_files_in_patch"
	local tmpfile=`mktemp`
	while read line
	do
		echo $line | grep -qE "*\.patch$" || continue
		git apply --numstat $line | awk '{print $NF}' >> $tmpfile
	done < $diff_files_in_tree
	sort -u $tmpfile > $diff_files_in_patch
	rm -f $tmpfile

	[ `wc -l $diff_files_in_patch | awk '{print $1}'` -eq 0 ] && return
	setup_rpmdev_tree || {
		echo "ERROR: setup rpmdev tree failed."
		return 1
	}
	while read line
	do
		file_name=${line##*/}
		find $build_dir -name $file_name | grep $line >> $diff_files_to_check
	done < $diff_files_in_patch
	return 0
}

# check pkg installed or not
check_pkg_inst() {
	local ret
	for pkg in $(ls $PKG_CI_TEST_PATH); do
		cd $PKG_CI_TEST_PATH
		pkg_name=$(rpm -q $pkg --info | grep -E "^Name" | awk '{print $3}')
		rpm -q $pkg_name >&/dev/null
		ret=$?
		PKG_INSTALL_HASH[$pkg]=$ret
		cd -
	done
}

# install pkg
inst_pkg() {
	local pkg_name
	pkg_name=$1
	yum localinstall -y --skip-broken $pkg_name || {
		echo "ERROR: rpm $pkg_name install failed."
		return 1
	}
	return 0
}

# uninstall pkg
uninst_pkg() {
	local pkg_name pkg_install_flag
	pkg_name=$1
	pkg_install_flag=$2
	if [ $pkg_install_flag -ne 0 ]; then
		yum erase -y $pkg_name || {
			echo "ERROR: rpm $pkg_name uninstall failed."
			return 1
		}
	fi
	return 0
}

# get binary file if pkg contain any binary files
gen_bin_file() {
	local pkg_name
	pkg_name=$1
	rpm -ql $pkg_name | grep -E "/bin/|/sbin/" | grep -Ev "/etc/|/libexec/|/opt/" >&$BIN_FILE_LIST || {
		rm -rf $BIN_FILE_LIST
		return 1
	}
	return 0
}

# check ldd binary file
ldd_check() {
	local exist_flag
	exist_flag=0
	[ -n "$BIN_FILE_LIST" ] && [ -f "$BIN_FILE_LIST" ] || return 1
	while read line; do
		ldd $line | grep "not found" && {
			echo "Error: ldd check failed on $line"
			((exist_flag++))
		}
	done <$BIN_FILE_LIST
	return $exist_flag
}

# check binary file help cmd
check_help() {
	local bin=$1
	timeout -s SIGKILL 2s $bin --help && return 0
	timeout -s SIGKILL 2s $bin -h && return 0
	timeout -s SIGKILL 2s $bin --help 2>&1 | grep -i "usage\|options" && return 0
	timeout -s SIGKILL 2s $bin -h 2>&1 | grep -i "usage\|options" && return 0
	return 1
}

# check binary file version cmd
check_version() {
	local bin=$1
	timeout -s SIGKILL 2s $bin --version && return 0
	timeout -s SIGKILL 2s $bin -v && return 0
	timeout -s SIGKILL 2s $bin -V && return 0
	timeout -s SIGKILL 2s $bin --usage && return 0
	return 1
}

# check binary excecution
exec_check() {
	local exist_flag
	exist_flag=0
	while read line; do
		check_help $line || check_version $line || {
			echo "Error: exec check failed on $line"
			((exist_flag++))
		}
	done <$BIN_FILE_LIST
	return $exist_flag
}

# check pkg's services
service_check() {
	local pkg_name service_flag service_path service_file exist_flag
	pkg_name=$1
	exist_flag=0
	service_path=$(rpm -ql $pkg_name | grep /usr/lib/systemd/system/.*.service | grep -v @)
	service_flag=$?
	service_file=$(echo $service_path | awk -F '/' '{print $NF}')
	if [ $service_flag -eq 0 ]; then
		for service in $service_file; do
			grep ExecReload $service_path && SERVICE_ACTION+=("reload")
			for ((i = 0; i < ${#SERVICE_ACTION[@]}; i++)); do
				systemctl ${SERVICE_ACTION[i]} $service --no-pager || {
					echo "Error: service check failed on $service"
					((exist_flag++))
				}
			done
		done
	fi
	return $exist_flag
}

get_rpm_pkg() {
	local RPM_ARR
	mkdir -p $PKG_CI_TEST_PATH
	if [ -z "$PKG_CI_ABS_RPM_URL" ]; then
		echo "Error: PKG_CI_ABS_RPM_URL is Null."
		return 1
	fi
	RPM_ARR=${PKG_CI_ABS_RPM_URL//,/ }
	for i in ${RPM_ARR[@]}; do
		echo $i | grep -E "($(uname -m)|noarch)\.rpm" && {
			wget $i -P $PKG_CI_TEST_PATH >&/dev/null || {
				echo "Error: wget $i failed"
				return 1
			}
		}
	done
	return 0
}

pkg_smoke_test() {
	local rpm_pkg pkg_name exist_flag os_version pkg_names skip_dg_pkgs
	[ -n "$PKG_CI_ABS_RPM_URL" ] || {
            fail "$FUNCNAME"
            die "ERROR: PKG_CI_ABS_RPM_URL is null."
        }
	which yum-config-manager &>/dev/null || {
		yum_install yum-utils || {
			skip "$FUNCNAME"
			echo "ERROR: install yum-utils failed."
			return 0
		}
	}
	get_rpm_pkg || {
		fail "$FUNCNAME"
		rm -rf $PKG_CI_TEST_PATH $BIN_FILE_LIST
		die "ERROR: Download rpm pkgs failed."
	}
	ls $PKG_CI_TEST_PATH/*.rpm || {
		skip "$FUNCNAME" 
		echo "Warning: There is no pkg in $PKG_CI_TEST_PATH to be test."
		return 0
	}
	check_pkg_inst
	exist_flag=0
	# install old pkgs
	ls /etc/yum.repos.d/build.repo &>/dev/null && yum-config-manager --disable build &>/dev/null
	pkg_names=""
	skip_dg_pkgs=""
	for rpm_pkg in $(ls $PKG_CI_TEST_PATH); do
		pkg_name=$(rpm -q "$PKG_CI_TEST_PATH"/"$rpm_pkg" --info | grep -E "^Name" | awk '{print $3}')
		if [ -n "$pkg_name" ]; then
			yum install -y $pkg_name --nogpgcheck || {
				echo "=====install old pkg $pkg_name failed, skip downgrade it."
				skip_dg_pkgs="$skip_dg_pkgs $pkg_name"
				continue
			}
			pkg_names="$pkg_names $pkg_name"
		fi
	done
	ls /etc/yum.repos.d/build.repo &>/dev/null && yum-config-manager --enable build &>/dev/null
	# install/upgrade pkgs
	echo "=====begin to install/upgrade pkgs."
	yum localinstall -y $PKG_CI_TEST_PATH/*.rpm --nogpgcheck || ((exist_flag++))
	# downgrade pkgs
	if [ -n "$pkg_names" ]; then
		# erase skip downgrade pkgs first.
		echo "=====remove the undowngrade pkg: $skip_dg_pkgs."
		yum erase -y $skip_dg_pkgs || ((exist_flag++))
		echo "=====begin to downgrade $pkg_names"
		downgrade_cmd="yum downgrade -y $pkg_names --nogpgcheck"
		$downgrade_cmd || {
			echo "Error: downgrade pkg failed."
			((exist_flag++))
		}
	fi
	# uninstall pkgs
	os_version=$(cat /etc/os-release | grep ID= |awk -F'"' '{print $2}'|xargs)
	if ! echo $os_version | grep "anolis 7.9"; then
		for rpm_pkg in $(ls $PKG_CI_TEST_PATH); do
			pkg_name=$(rpm -q "$PKG_CI_TEST_PATH"/"$rpm_pkg" --info | grep -E "^Name" | awk '{print $3}')
			if [ -n "$pkg_name" ]; then
				uninst_pkg $pkg_name ${PKG_INSTALL_HASH[$rpm_pkg]} || ((exist_flag++))
			fi
		done
	fi
	rm -rf $PKG_CI_TEST_PATH $BIN_FILE_LIST
	[ $exist_flag -eq 0 ] && pass "$FUNCNAME" || fail "$FUNCNAME"
}

# create pkg_name to pkg_rpm_name hash table
create_hash() {
	local rpm_dir hash_name
	rpm_dir=$1
	hash_name=$2
	cd $rpm_dir
	for pkg in $(ls $rpm_dir | grep -E "*.rpm"); do
		pkg_name=$(rpm -q --qf "%{name}" $pkg)
		eval "$hash_name"[$pkg_name]=$pkg
	done
	cd -
}

# get rpms' list
get_rpm_list() {
	local RPM_ARR spec_file
	which cpio &>/dev/null || yum_install  cpio || return 1
	which rpmspec &>/dev/null || yum_install  rpm-build || return 1
	rm -rf $RPM_DIR_OLD/*
	mkdir -p $RPM_DIR_OLD
	RPM_ARR=${PKG_CI_ABS_RPM_URL//,/ }
	for i in ${RPM_ARR[@]}; do
		echo $i | grep -E "*.src.rpm" && {
			wget $i -P $RPM_DIR_OLD >&/dev/null || {
				echo "Error: wget $i failed"
				return 1
			}
		}
	done
	cd $RPM_DIR_OLD
	rpm2cpio *.src.rpm | cpio -iv || return 1
	spec_file=$(ls *.spec)
	rpmspec -q --qf '%{name}\n' $spec_file >$RPM_PKG_LIST || return 1
	cd -
	rm -rf $RPM_DIR_OLD/*
	return 0
}

# download old rpm pkgs
get_old_rpm_pkg() {
	which yumdownloader &>/dev/null || yum_install  yum-utils || return 1
	
	# enable debug repo
	cp -a /etc/yum.repos.d/AnolisOS-Debuginfo.repo /etc/yum.repos.d/AnolisOS-Debuginfo.repo.bak
	sed -i 's/enabled=0/enabled=1/g' /etc/yum.repos.d/AnolisOS-Debuginfo.repo
	yum makecache

	# download pkgs
	while read pkg; do
		yumdownloader --destdir=$RPM_DIR_OLD $pkg
	done <$RPM_PKG_LIST

	# recover debug repo
	mv /etc/yum.repos.d/AnolisOS-Debuginfo.repo.bak /etc/yum.repos.d/AnolisOS-Debuginfo.repo
	return 0
}

check_rpmsodiff() {
	os_version=$(cat /etc/os-release | grep ID= |awk -F'"' '{print $2}'|xargs)
	if echo $os_version | grep "anolis 23"; then
		which rpmsodiff &>/dev/null || {
			yum_install  rpmdevtools || {
				echo "ERROR: install rpmdevtools failed."
				return 1
			}
		}
		return 0
	fi
	return 1
}

# check abi diff test
check_abi_diff() {
	local exist_flag exec_flag so_diff_flag
	# check repo source url black list
	grep $PKG_CI_REPO_SOURCE_URL $util_dir/black_list/check_abi_diff.list && { skip "$FUNCNAME"; return 0; }
	[ -n "$PKG_CI_ABS_RPM_URL" ] || {
		fail "$FUNCNAME"
		die "ERROR: PKG_CI_ABS_RPM_URL is null."
	}
	yum install -y crypto-policies --nogpgcheck || {
		skip "$FUNCNAME"
		echo "Warning: crypto-policies is not been installed/upgraded correctly, please check."
		return 0
	}
	get_rpm_list
	get_old_rpm_pkg || {
		fail "$FUNCNAME"
		die "ERROR: download old rpms failed."
	}
	get_rpm_pkg || {
		fail "$FUNCNAME"
		rm -rf $PKG_CI_TEST_PATH $BIN_FILE_LIST
		die "ERROR: Download rpm pkgs failed."
	}
	exist_flag=0
	exec_flag=1000
	which abipkgdiff &>/dev/null || {
			yum_install  libabigail || {
			yum --enablerepo=epel -y install libabigail >&/dev/null || {
				skip "$FUNCNAME" 
				echo "Warning: abipkgdiff is not been installed correctly, please check."
				return 0
			}
		}
	}

	check_rpmsodiff
	so_diff_flag=$?

	create_hash $RPM_DIR_OLD PKG_HASH_OLD
	create_hash $PKG_CI_TEST_PATH PKG_HASH_NEW
	while read line; do
		echo $line | grep -qE "\-devel|-debug|-docs|-doc" && continue
		[ -n "${PKG_HASH_OLD[$line]}" ] || {
			echo "Not find old rpm"
			continue
		}
		[ -n "${PKG_HASH_NEW[$line]}" ] || {
			echo "Not find new rpm"
			continue
		}
		cmd="abipkgdiff --no-added-syms --no-added-binaries --no-unreferenced-symbols --dso-only"
		[ -n "${PKG_HASH_OLD[${line}-debuginfo]}" ] && cmd="$cmd --d1 $RPM_DIR_OLD/${PKG_HASH_OLD[${line}-debuginfo]}"
		[ -n "${PKG_HASH_NEW[${line}-debuginfo]}" ] && cmd="$cmd --d2 $PKG_CI_TEST_PATH/${PKG_HASH_NEW[${line}-debuginfo]}"
		[ -n "${PKG_HASH_OLD[${line}-devel]}" ] && cmd="$cmd --devel1 $RPM_DIR_OLD/${PKG_HASH_OLD[${line}-devel]}"
		[ -n "${PKG_HASH_NEW[${line}-devel]}" ] && cmd="$cmd --devel2 $PKG_CI_TEST_PATH/${PKG_HASH_NEW[${line}-devel]}"
		echo "$cmd $RPM_DIR_OLD/${PKG_HASH_OLD[$line]} $PKG_CI_TEST_PATH/${PKG_HASH_NEW[$line]}"
		$cmd $RPM_DIR_OLD/${PKG_HASH_OLD[$line]} $PKG_CI_TEST_PATH/${PKG_HASH_NEW[$line]} || {
			((exist_flag++))
		}
		exec_flag=$?
		
		if [ $so_diff_flag -eq 0 ]; then
			echo "rpmsodiff $RPM_DIR_OLD/${PKG_HASH_OLD[$line]} $PKG_CI_TEST_PATH/${PKG_HASH_NEW[$line]}"
			rpmsodiff $RPM_DIR_OLD/${PKG_HASH_OLD[$line]} $PKG_CI_TEST_PATH/${PKG_HASH_NEW[$line]} || {
				((exist_flag++))
			}
		fi
	done <$RPM_PKG_LIST
	rm -rf $PKG_CI_TEST_PATH $RPM_DIR_OLD $RPM_PKG_LIST
	if [ "$exist_flag" -eq 0 ]; then
		if [ "$exec_flag" -ne 1000 ]; then
			pass "$FUNCNAME" 
		else
			skip "$FUNCNAME" 
		fi
	else
		fail "$FUNCNAME"
	fi
}

check_pkg_dependency () {
	local exist_flag
	exist_flag=0
	os_version=$(cat /etc/os-release | grep ID= |awk -F'"' '{print $2}'|xargs)
	if ! echo $os_version | grep "anolis 23"; then
		skip "$FUNCNAME" 
		echo "ERROR: Only an23 do the pkg dependency check."
		return 0
	fi
	yum install -y crypto-policies --nogpgcheck || {
		skip "$FUNCNAME"
		echo "Warning: crypto-policies is not been installed/upgraded correctly, please check."
		return 0
	}
	which rpmdeplint &>/dev/null || {
		yum_install rpmdeplint || {
			skip "$FUNCNAME" 
			echo "ERROR: install rpmdeplint failed."
			return 0
		}
	}
	which yum-config-manager &>/dev/null || {
		yum_install yum-utils || {
			skip "$FUNCNAME" 
			echo "ERROR: install yum-utils failed."
			return 0
		}
	}
	rpm -q python3-setuptools &>/dev/null || {
                yum_install python3-setuptools || {
                        skip "$FUNCNAME"
                        echo "ERROR: install python3-setuptools failed."
                        return 0
                }
        }
	yum-config-manager --disable build
	get_rpm_pkg || {
		fail "$FUNCNAME"
		rm -rf $PKG_CI_TEST_PATH
		die "ERROR: Download rpm pkgs failed."
	}
	ls $PKG_CI_TEST_PATH/*.rpm || {
		skip "$FUNCNAME" 
		echo "Warning: There is no pkg in $PKG_CI_TEST_PATH to check."
		return 0
	}
	cmd="$PKG_CI_TEST_PATH/*.rpm"
	if ! ls $PKG_CI_TEST_PATH | grep ".$(uname -m).rpm"; then
		cmd="$PKG_CI_TEST_PATH/*.rpm --arch noarch"
	fi
	repo_param="--repo koji,http://8.131.87.1/kojifiles/repos/dist-an23-build/latest/$(uname -m)/"
	yum clean all
	for ((i = 0; i < ${#RPMDEPLINT_PARAM[@]}; i++)); do
		tmplog=$(mktemp)
		echo "rpmdeplint ${RPMDEPLINT_PARAM[i]} $repo_param $cmd"
		rpmdeplint ${RPMDEPLINT_PARAM[i]} $repo_param $cmd &>$tmplog || {
			((exist_flag++))
			cat $tmplog
		}
		rm -f $tmplog
	done
	yum-config-manager --enable build
	rm -rf $PKG_CI_TEST_PATH
	if [ "$exist_flag" -eq 0 ]; then
		pass "$FUNCNAME"
	else
		fail "$FUNCNAME"
	fi
}

creat_anolis23_build_repo() {
	cat > /etc/yum.repos.d/build.repo <<EOF
[build]
name=build
baseurl=http://8.131.87.1/kojifiles/repos/dist-an23-build/latest/$(uname -m)/
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ANOLIS
gpgcheck=0
EOF
	yum makecache
}

# check if repo of rpm source tree
is_rpm_tree()
{
	[ -n "$PKG_CI_REPO_GROUP" ] && [[ "$PKG_CI_REPO_GROUP" =~ ^src-anolis-* ]] && return 0
	return 1
}
